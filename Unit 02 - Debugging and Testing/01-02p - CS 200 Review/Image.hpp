#ifndef _IMAGE_HPP
#define _IMAGE_HPP

#include <string>
using namespace std;

const int IMAGE_WIDTH = 300;
const int IMAGE_HEIGHT = 300;

struct Pixel
{
    int r, g, b;
};

class PpmImage
{
    public:
    PpmImage();
    ~PpmImage();

    bool LoadImage( const string& filename );
    bool SaveImage( const string& filename );

    // Filters:
    void ApplyFilter1();
    void ApplyFilter2();
    void ApplyFilter3();
    void ApplyFilter4();

    // Assignment operator:
    void operator= ( const PpmImage& other );

    private:
    int m_colorDepth;
    Pixel** m_pixels; // Dynamic 2D array

    bool ValidateImageFormat();
};

#endif
