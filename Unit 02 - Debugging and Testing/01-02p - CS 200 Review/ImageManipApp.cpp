#include "ImageManipApp.hpp"

#include "utilities/Menu.hpp"
#include "utilities/System.hpp"
#include "utilities/StringUtil.hpp"

#include <iostream>
#include <string>
using namespace std;

ImageManipApp::ImageManipApp()
{
    SetImagePath( "../images/" );
    m_loadedImageName = "";
}

ImageManipApp::ImageManipApp( string imagePath )
{
    SetImagePath( imagePath );
    m_loadedImageName = "";
}

void ImageManipApp::SetImagePath( string imagePath )
{
    m_imagePath = imagePath;
}

void ImageManipApp::Startup()
{
    Menu::Header( "Image Manipulation Program" );

    cout << "Image path set to: " << m_imagePath << endl << endl;
    cout << "Files in this directory: " << endl;
    System::DisplayDirectoryContents( m_imagePath );
    cout << endl;
    cout << "If the image files don't show up here, "
         << "please update the " << endl << "ImageManipApp app( \"../images/\" ); " << endl << "in main()." << endl;
    Menu::Pause();
}

void ImageManipApp::MainMenu()
{
    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "Main Menu" );

        DisplayLoadedImage();
        DisplayAppliedFilters();

        cout << endl;
        string choice = Menu::ShowStringMenuWithPrompt( {
            "Load image",
            "Apply filter",
            "Save filtered image",
            "Quit"
        } );

        if ( choice == "Load image" )
        {
            LoadImage();
        }
        else if ( choice == "Apply filter" )
        {
            ApplyFilter();
        }
        else if ( choice == "Save filtered image" )
        {
            SaveImage();
        }
        else if ( choice == "Quit" )
        {
            done = true;
        }
    }
}

void ImageManipApp::Run()
{
    Startup();
    MainMenu();
}

void ImageManipApp::LoadImage()
{
    Menu::Header( "Load image" );

    cout << "Files in the " << m_imagePath << " directory: " << endl;
    System::DisplayDirectoryContents( m_imagePath );

    cout << "Enter filename (no .ppm): ";
    getline( cin, m_loadedImageName );

    string fullPath = m_imagePath + m_loadedImageName + ".ppm";

    bool result = m_loadedImage.LoadImage( fullPath );
    if ( result == false )
    {
        cout << "Failed to open file \"" << result << "\"!" << endl;
        m_loadedImageName = "";
    }
    else
    {
        cout << " File OPENED successfully."
             << "\n Note that this might not mean that it was LOADED correctly; You may need to"
             << "\n debug your work if your saved filtered image doesn't come out right." << endl;
    }

    m_filteredImage = m_loadedImage;
    m_appliedFilters.clear();

    Menu::Pause();
}

void ImageManipApp::ApplyFilter()
{
    Menu::Header( "Apply filter" );

    int choice = Menu::GetValidChoice( 1, 4, "Apply which filter?" );

    switch( choice )
    {
        case 1:
        m_appliedFilters.push_back( 1 );
        m_filteredImage.ApplyFilter1();
        break;

        case 2:
        m_appliedFilters.push_back( 2 );
        m_filteredImage.ApplyFilter2();
        break;

        case 3:
        m_appliedFilters.push_back( 3 );
        m_filteredImage.ApplyFilter3();
        break;

        case 4:
        m_appliedFilters.push_back( 4 );
        m_filteredImage.ApplyFilter4();
        break;
    }

    cout << "Filter applied." << endl;

    Menu::Pause();
}

void ImageManipApp::SaveImage()
{
    string fullPath = m_imagePath + BuildImageFilename() + ".ppm";
    bool result = m_filteredImage.SaveImage( fullPath );

    if ( result == false )
    {
        cout << "Error writing out file to \"" << fullPath << "\"!" << endl;
    }
    else
    {
        cout << " File OPENED successfully."
             << "\n Note that this might not mean that it saved correctly; If the file doesn't"
             << "\n open in an image editor, open the image file in a TEXT EDITOR and"
             << "\n manually check to see if the file is in the correct format." << endl;
    }

    Menu::Pause();
}

string ImageManipApp::ConvertPath( string path, string userInput )
{
    if ( userInput.find( ".ppm" ) == string::npos )
    {
        // You didn't type the file extension!!
        // That's ok... I'll add it for you!
        userInput += ".ppm";
    }

    return path + userInput;
}

void ImageManipApp::DisplayLoadedImage()
{
    cout << "Loaded image: ";
    if ( m_loadedImageName == "" ) { cout << "None"; }
    else { cout << m_loadedImageName; }
    cout << endl;
}

void ImageManipApp::DisplayAppliedFilters()
{
    cout << "Applied filters: ";
    for ( unsigned int i = 0; i < m_appliedFilters.size(); i++ )
    {
        if ( i != 0 ) { cout << ", "; }
        cout << m_appliedFilters[i];
    }
    cout << endl;
}

string ImageManipApp::BuildImageFilename()
{
    string name = m_loadedImageName;
    for ( auto& filter : m_appliedFilters )
    {
        name += StringUtil::ToString( filter );
    }
    return name;
}

