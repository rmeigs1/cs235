#include "ImageManipApp.hpp"

int main()
{
    ImageManipApp app;

    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        // YOU'RE RUNNING WINDOWS
        app.SetImagePath( "..\\images\\" );
    #else
        // YOU'RE RUNNING LINUX, UNIX, OR MAC
        app.SetImagePath( "../images/" );
    #endif

    app.Run();

    return 0;
}

