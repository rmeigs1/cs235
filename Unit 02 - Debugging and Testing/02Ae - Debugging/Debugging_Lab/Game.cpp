#include "Game.hpp"

#include "Utilities.hpp"

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

Game::Game()
{
    // Seed the random number generator
    srand( time( NULL ) );

    // Set the starting program state
    m_gameState = MAIN_MENU;

    m_score = 0;

    LoadGame();
}

Game::~Game()
{
    SaveGame();
}

void Game::Run()
{
    GameState nextState = m_gameState;

    while ( nextState != QUIT )
    {
        if      ( nextState == MAIN_MENU )  { nextState = MainMenu(); }
        else if ( nextState == GAME )       { nextState = Gameplay(); }
    }

    cout << "Game saved." << endl;
}

GameState Game::MainMenu()
{
    ClearScreen();

    cout << "TTTTT RRR   EEEE  ZZZZZ  OOOO  RRR    OOOO" << endl;
    cout << "  T   R  R  E        Z   O  O  R  R   O  O" << endl;
    cout << "  T   RRR   EEEE   ZZ    O  O  RRR    O  O" << endl;
    cout << "  T   R  R  E     Z      O  O  R  R   O  O" << endl;
    cout << "  T   R   R EEEE  ZZZZZ  OOOO  R   R  OOOO" << endl;
    cout << "------------------------------------------" << endl;
    cout << "1. Play" << endl;
    cout << "2. Instructions" << endl;
    cout << "3. Quit" << endl;
    int choice = GetIntInput( 1, 3 );

    if ( choice == 1 )
    {
        return GAME;
    }
    else if ( choice == 3 )
    {
        return QUIT;
    }
    else
    {
        Instructions();
        return MAIN_MENU;
    }
}

GameState Game::Gameplay()
{
    bool done = false;
    m_map.GenerateMap();

    while ( !done )
    {
        ClearScreen();
        m_map.Draw();
        DrawHud();

        string choice = GetStringInput();
        m_map.MovePlayer( choice );

        if ( choice == "QUIT" )
        {
            done = true;
        }

        if ( m_map.PlayerCollectTreasure() )
        {
            m_score++;
            return YouWin();
        }
        else if ( m_map.GoblinGetPlayer() )
        {
            return YouLose();
        }
    }

    return MAIN_MENU;
}

GameState Game::YouWin()
{
    ClearScreen();
    cout << "-------------------------------------------------" << endl;
    cout << "-              Y O U       W I N !              -" << endl;
    cout << "-                                               -" << endl;
    cout << "- You have found " << m_score << " treasures. \t\t\t-" << endl;
    cout << "-------------------------------------------------" << endl;

    cout << " Continue to the next level? (y/n): ";
    string input = GetStringInput();
    if ( input == "n" )
    {
        return MAIN_MENU;
    }
    else
    {
        return GAME;
    }
}

GameState Game::YouLose()
{
    ClearScreen();
    cout << "-------------------------------------------------" << endl;
    cout << "-                   O H   N O !                 -" << endl;
    cout << "-                                               -" << endl;

    if ( m_score == 0 )
    {
        cout << "- The goblin got you, but you have no treasure! -" << endl;
    }
    else
    {
        m_score--;
        cout << "- The goblin stole one of your treasures!       -" << endl;
    }

    cout << "-------------------------------------------------" << endl;

    cout << " Continue to the next level? (y/n): ";
    string input = GetStringInput();
    if ( input == "n" )
    {
        return MAIN_MENU;
    }
    else
    {
        return GAME;
    }
}

void Game::Instructions()
{
    cout << "Use the [W, A, S, D] keys to move around the labyrinth." << endl;
    cout << "Find treasure and collect it to win." << endl;
    cout << endl;
    cout << "Go back? (y/n): ";
    GetStringInput();
}

void Game::DrawHud()
{
    cout << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "- GOAL: Move yourself (@) to           SCORE:   -" << endl;
    cout << "-       collect the treasure ($)!      " << m_score << "\t-" << endl;
    cout << "-       Avoid the goblin (&)!                   -" << endl;
    cout << "-                                               -" << endl;
    cout << "-                " << m_map.MoveUpKey() << ": Move NORTH                  -" << endl;
    cout << "- " << m_map.MoveLeftKey() << ": Move WEST   " << m_map.MoveDownKey() << ": Move SOUTH   " << m_map.MoveRightKey() << ": Move EAST   -" << endl;
    cout << "-                                               -" << endl;
    cout << "- Or type QUIT to quit.                         -" << endl;
    cout << "-------------------------------------------------" << endl;
}


void Game::SaveGame()
{
    ofstream output( "save.txt" );
    output << m_score;
}

void Game::LoadGame()
{
    ifstream input( "save.txt" );
    input >> m_score;
}
