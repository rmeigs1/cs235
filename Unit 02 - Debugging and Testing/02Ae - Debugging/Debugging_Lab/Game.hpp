#ifndef _GAME_HPP
#define _GAME_HPP

#include "Map.hpp"

enum GameState { MAIN_MENU, GAME, QUIT };

class Game
{
    public:
    Game();
    ~Game();

    void Run();

    private:
    GameState m_gameState;
    Map m_map;
    int m_score;

    GameState MainMenu();
    GameState Gameplay();
    GameState YouWin();
    GameState YouLose();
    void Instructions();

    void DrawHud();

    void SaveGame();
    void LoadGame();
};

#endif
