#include "Utilities.hpp"

#include <iostream>
using namespace std;

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    // The terminal clear command on Windows is "cls"
    system( "cls" );
    #else
    // The terminal clear command on Linux/Mac/Unix is "clear"
    system( "clear" );
    #endif
}

int GetIntInput( int minimum, int maximum )
{
    cout << ">> ";
    int val;
    cin >> val;
    while ( val < minimum || val > maximum )
    {
        cout << "Invalid choice." << endl;
        cout << ">> ";
        cin >> val;
    }
    return val;
}

string GetStringInput()
{
    cout << ">> ";
    string val;
    cin >> val;
    return val;
}
