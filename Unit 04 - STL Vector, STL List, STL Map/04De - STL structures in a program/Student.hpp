#ifndef _STUDENT_HPP
#define _STUDENT_HPP

#include <string>
#include <iostream>
#include <vector>
using namespace std;

class Student
{
public:
    void Setup( string studentId );
    void AddCourse( string courseName );
    void Display();

    void SetName( string name );
    void SetStudentId( string studentId );

private:
    string m_fullName;
    string m_studentId;
    // TODO: Add vector of strings named m_courses
};

void Student::Setup( string studentId )
{
    m_studentId = studentId;

    cin.ignore();
    cout << "Enter student's name: ";
    getline( cin, m_fullName );

    int courseCount;
    cout << "How many courses? ";
    cin >> courseCount;
    cin.ignore();

    for ( int i = 0; i < courseCount; i++ )
    {
        string course;
        cout << "Enter course " << ( i + 1 ) << ": ";
        getline( cin, course );
        AddCourse( course );
    }
}

void Student::AddCourse( string courseName )
{
    // TODO: Add courseName to m_courses vector
}

void Student::Display()
{
    cout << "STUDENT: \t" << m_fullName << endl;
    cout << "STU ID: \t" << m_studentId << endl;
    cout << "COURSES:";
    // TODO: Iterate through m_courses, displaying each course
    cout << endl << endl;
}


void Student::SetName( string name )
{
    m_fullName = name;
}

void Student::SetStudentId( string studentId )
{
    m_studentId = studentId;
}

#endif
