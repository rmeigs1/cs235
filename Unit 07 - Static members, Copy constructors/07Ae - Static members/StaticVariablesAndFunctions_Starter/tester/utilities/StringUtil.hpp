#ifndef _STRINGS
#define _STRINGS

#include <string>
#include <sstream>
using namespace std;

class StringUtil
{
    public:
    template <typename T>
    static string ToString( const T& value )
    {
        stringstream ss;
        ss << value;
        return ss.str();
    }


    static int StringToInt( const string& str )
    {
        istringstream ss( str );
        int val;
        ss >> val;
        return val;
    }

    static string ToUpper( const string& val )
    {
        string upper = "";
        for ( unsigned int i = 0; i < val.size(); i++ )
        {
            upper += toupper( val[i] );
        }
        return upper;
    }

    static string ToLower( const string& val )
    {
        string upper = "";
        for ( unsigned int i = 0; i < val.size(); i++ )
        {
            upper += tolower( val[i] );
        }
        return upper;
    }

    static string ColumnText( int colWidth, const string& text )
    {
        string adjusted = text;
        for ( int i = 0; i < colWidth - text.size(); i++ )
        {
            adjusted += " ";
        }
        return adjusted;
    }
};



#endif
