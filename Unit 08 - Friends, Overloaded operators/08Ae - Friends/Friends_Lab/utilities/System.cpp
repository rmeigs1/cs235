#include "System.hpp"

#include <chrono>
#include <thread>

void System::Sleep(int delay)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));
}
